package main

import (
	"net/http"
	"os"
	"time"
)

func main() {
	url := getenv("HEALTHCHECK_URL", "http://127.0.0.1")
	timeout := getenv("HEALTHCHECK_TIMEOUT", "30s")
	d, _ := time.ParseDuration(timeout)
	check(url, d)
}

func check(url string, duration time.Duration) {
	timeout := time.Duration(duration)

	client := http.Client{
		Timeout: timeout,
	}

	resp, err := client.Get(url)

	if err != nil || resp.StatusCode != 200 {
		os.Exit(1)
	}
}

func getenv(key string, value string) string {
	v := os.Getenv(key)
	if len(v) == 0 {
		v = value
	}
	return v
}
