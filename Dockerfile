FROM docker.io/golang:alpine as builder

RUN mkdir /go/src/healthcheck

WORKDIR /go/src/healthcheck

COPY healthcheck.go /go/src/healthcheck

RUN go mod init healthcheck && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"'

FROM scratch

COPY --from=builder /go/src/healthcheck/healthcheck /healthcheck
